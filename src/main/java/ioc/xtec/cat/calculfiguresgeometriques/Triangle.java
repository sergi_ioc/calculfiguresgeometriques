/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;
/**
 *
 * @author sergi
 */

public class Triangle implements FiguraGeometrica {
    private final double base;
    private final double altura;
    private final double costat1;
    private final double costat2;
    private final double costat3;

    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la base del triangle: ");
        this.base = scanner.nextDouble();
        System.out.println("Introduïu l'altura del triangle: ");
        this.altura = scanner.nextDouble();
        System.out.println("Introduïu el primer costat del triangle: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu el segon costat del triangle: ");
        this.costat2 = scanner.nextDouble();
        System.out.println("Introduïu el tercer costat del triangle: ");
        this.costat3 = scanner.nextDouble();
    }

    @Override
    public double calcularArea() {
        return (base * altura) / 2;
    }

    @Override
    public double calcularPerimetre() {
        return costat1 + costat2 + costat3;
    }
}
